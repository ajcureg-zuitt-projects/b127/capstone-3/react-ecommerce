import {useState, useEffect} from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';
import {Container} from 'react-bootstrap';
import UserContext from './UserContext'
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProduct';
import Cart from './pages/Cart';
import Orders from './pages/Orders';
import Error from './pages/Error';


function App() {

  const [user, setUser] = useState({
                accessToken: localStorage.getItem('accessToken'),
                email:localStorage.getItem('email'),
                isAdmin: localStorage.getItem('isAdmin') === 'true',
              });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage)
  },[user])

  return (
    <UserContext.Provider value={ {user, setUser, unsetUser} }>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/products/:productId" component={SpecificProduct} />
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/orders" component={Orders} />
            <Route component={Error} />
          </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
  )
}
export default App;