import {useState,useEffect, useContext} from 'react';
import {Container} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import UserCart from '../components/UserCart';

export default function Cart () {
	const {user} = useContext(UserContext);
	const [cartContent, setCartContent] = useState([]);
	const [totalAll, setTotalAll] = useState([]);
	
	const fetchProduct = () =>{
		fetch('http://localhost:4000/cart/myCart', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res=>res.json())
		.then(data=> {
			if (data.length!==0) {
				const contentArray = data[0].products
				setCartContent(contentArray)
				setTotalAll(data[0].totalPrice)
			} else {
				setCartContent([])
			}
		})
	}
	useEffect(()=> {
		fetchProduct()
	},[])
	return(
		user.accessToken==null?
		<div className='d-flex justify-content-center pt-5'>
			<h4>Please <Link to='/login'>login</Link> to view your cart.</h4>
		</div>
		
		:
		<Container>
			{
				<UserCart productData={cartContent} productPrice={totalAll} fetchProduct={fetchProduct}/>
			}
		</Container>
	)
}