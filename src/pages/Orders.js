import {useState,useEffect, useContext, Fragment} from 'react';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import UserOrder from '../components/UserOrder';

export default function Cart () {
	const {user} =useContext(UserContext);
	const [orders, setOrders] = useState([]);

	const fetchOrder = () =>{
		fetch('http://localhost:4000/orders/myOrders', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res=>res.json())
		.then(data=> {
			if (data.length!==0) {
				const orderArr = data.map(order=>{
					return <UserOrder orderData={order}/>
				})
				setOrders(orderArr);
			} else {
				setOrders();
			}
		})
	}

	useEffect(()=> {
		fetchOrder()
	},[])

	return(
		user.accessToken==null?
		<div className='d-flex justify-content-center pt-5'>
			<h4>Please <Link to='/login'>login</Link> to order.</h4>
		</div>
		:
		orders==null?
		<div className='text-center pt-5'>
			<h4>No current orders.</h4>
			<p>Click <Link to='/products'>here</Link> to view available products.</p>
		</div>
		:
		<Fragment>
			{orders}
		</Fragment>
	)
}