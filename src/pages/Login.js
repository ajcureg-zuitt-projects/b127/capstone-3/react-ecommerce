import {Fragment, useEffect, useState, useContext} from 'react';
import {Redirect, useHistory} from 'react-router-dom'
import UserContext from '../UserContext'
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login () {
	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	
	const [isActive,setIsActive] = useState(false);
	const history = useHistory();

	useEffect(()=> {
		if (email!=='' && password!=='') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[email, password]);

	let loginUser = e => {
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res=>res.json())
		.then(data=> {
			console.log(data)
			if (data.accessToken!==undefined) {
				localStorage.setItem('accessToken', data.accessToken);
				setUser({'accessToken':data.accessToken});

				Swal.fire({
					title: 'Log-In Successful!',
					icon: 'success',
				})

				fetch('http://localhost:4000/users/checkDetail', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res=>res.json())
				.then(data=> {
					console.log(data)
					if (data.isAdmin===true) {
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email:data.setEmail,
							isAdmin:data.isAdmin
						})
						history.push('/products');
					} else {
						history.push('/');
					}
				})
			} else {
				Swal.fire({
					title: 'Ooops!!',
					icon: 'error',
					text: 'Something went wrong. Check your credentials.'
				})
			}
			setEmail('');
			setPassword('');

		})
	}
	
		
	return(
		(user.accessToken !== null) ?
			<Redirect to="/"/>
			:
		<Fragment>
			<Form onSubmit ={e=>loginUser(e)}>
				<h1>Login</h1>
				<Form.Group>
					<Form.Label>Email address:</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter email"
						value={email}
						onChange= {e=>setEmail(e.target.value)}
						required
					/>
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter password"
						value={password}
						onChange= {e=>setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				{isActive ?
					<Button type="submit" variant="success" id="loginBtn">Login</Button>
					:
					<Button type="submit" variant="success" id="loginBtn" disabled>Login</Button>
				}

			</Form>
		</Fragment>
	)


}
