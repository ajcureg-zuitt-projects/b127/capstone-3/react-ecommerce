import {Fragment, useEffect, useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Redirect, useHistory} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register () {
	const {user} = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	

	const [isActive, setIsActive] = useState('');
	const history = useHistory();

	useEffect(()=>{
		if ((firstName!=='' && lastName!=='' && mobileNo.length>=11 && email!=='' && password1!=='' && password2!=='')) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[firstName, lastName, mobileNo, email, password1])

	let registerUser = e => {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method:'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if (data) {
				Swal.fire({
					title: `${email} already exist`,
					icon: 'error',
					text: 'Please use a different email.'
				})
				setPassword1('');
				setPassword2('');
			} else {
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName:firstName,
						lastName:lastName,
						mobileNumber: mobileNo,
						email:email,
						password:password1,
					})
				})
				.then(res=>res.json())
				.then(data=>{
					setFirstName('');
					setLastName('');
					setMobileNo('');
					setEmail('');
					setPassword1('');
					setPassword2('');

					if (data) {
						Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Yey! You are registered.'
						})
						history.push('/login')
					} else {
						Swal.fire({
							title: 'Ooops! Something went wrong',
							icon: 'error',
							text: 'Please try again later.'
						})
					}
				})
			}
		})
	}
console.log(mobileNo)

	return(
		(user.accessToken !== null)?
		<Redirect to='/'/>
		:
		<Fragment>
		<h1>Register</h1>

		<Form onSubmit={(e)=> registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="First Name"
					value={firstName}
					onChange={e=>setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Last Name"
					value={lastName}
					onChange={e=>setLastName(e.target.value)}
					required
				/>
			</Form.Group>
			
			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="text" 
					placeholder="Enter your Mobile Number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
					required
					/>
				</Form.Group>

			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e=>setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password"
					value={password1}
					onChange={e=>setPassword1(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your password with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify your password"
					value={password2}
					onChange={e=>setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ?
				<Button type="submit" variant="success" id="submitBtn">
				Submit
				</Button>
				:
				<Button type="submit" variant="success" id="submitBtn" disabled>
				Submit
				</Button>
			}
		</Form>
		</Fragment>
	)
}