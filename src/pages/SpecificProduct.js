import {useState, useEffect, useContext} from 'react';
import {Link, useHistory, useParams} from 'react-router-dom';
import {Container, Card, Button, Form, InputGroup} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function SpecificProduct () {
	const {user} = useContext(UserContext);
	const {productId} = useParams();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [qty, setQty] = useState(1);
	const history = useHistory();

	const incQty = e => {
		if (qty<99) {
			setQty(qty+1)
		}
	}

	const decQty = e => {
		if (qty>=1) {
			setQty(qty-1)
		}
	}
	

	useEffect(() => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res=>res.json())
		.then(data=> {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[])

	const addToCart = (productId,quantity) => {
		fetch(`http://localhost:4000/cart/addToCart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId:productId,
				quantity: quantity
			})
		})
		.then(res=> res.json())
		.then(data=> {
			if (data) {
				Swal.fire({
					title: 'Added to Cart',
					icon: 'success',
				})
			history.push('/products')
			} else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: `Something went wrong. Please try again`
				})
			}
		})
	}
	
	return(
		<Container className='d-flex justify-content-center mt-5'>
			<Card className='SpecificProduct'>
				<Card.Header className='bg-dark text-light text center'>
				{name}
				</Card.Header>
				<Card.Body>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Card.Subtitle>Qty:</Card.Subtitle>
					<InputGroup className='w-50 pt-3'>
						<InputGroup.Prepend>
					    	<Button variant="outline-danger" onClick={e=>decQty(e)}>-</Button>
					   	</InputGroup.Prepend>
					   	<Form.Control 
					   		className='text-center w-50' 
					   		value={qty} 
					   		type='text'
					   		maxLength='2' 
					   		onChange={e=>{
					   			if(e.target.value!=='') {
					   				setQty(parseInt(e.target.value))
					   			}
					   			else{
					   				setQty(0)
					   			}
					   		}}
					   	/>
						<InputGroup.Append>
							<Button variant="outline-primary" onClick={e=>incQty(e)}>+</Button>
						</InputGroup.Append>
					</InputGroup>
				</Card.Body>
				<Card.Footer>
				{
					user.accessToken !== null?
						<Button variant='primary' onClick={()=>addToCart(productId,qty)}>Add to Cart</Button>
						:
						<Link className='btn btn-primary' to='/login'>Login to add to cart</Link>
				}
				</Card.Footer>
			</Card>
		</Container>
	)
}
