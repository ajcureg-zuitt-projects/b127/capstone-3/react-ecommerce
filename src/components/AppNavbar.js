import {Fragment,useContext} from 'react'
import { Navbar, Nav} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const {user} = useContext(UserContext)

	let rightNav = (user.accessToken !== null)?
	<Fragment>
		<Nav.Link className="text-warning" as={NavLink} to="/logout">Logout</Nav.Link>
	</Fragment>
	:
	<Fragment >
		<Nav.Link className="text-light" as={NavLink} to ="/login">Login</Nav.Link>
		<Nav.Link className="text-light" as={NavLink} to ="/register">Register</Nav.Link>
	</Fragment>


	let leftNav = (user.isAdmin)?
	<Fragment>
		<Nav.Link className="text-light" as={NavLink} to ="/products">All Products</Nav.Link>
		<Nav.Link className="text-light" as={NavLink} to ="/orders">Orders</Nav.Link>
	</Fragment>
	:
	<Fragment >
		<Nav.Link className="text-light" as={NavLink} to ="/products">Products</Nav.Link>
		<Nav.Link className="text-light" as={NavLink} to ="/cart">Cart</Nav.Link>
		<Nav.Link className="text-light" as={NavLink} to ="/orders">Orders</Nav.Link>
	</Fragment>



	return(
		<Navbar bg="info" expand="lg">
			<Navbar.Brand className="text-light" as={Link} to ="/">MMT</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{leftNav}
				</Nav>
				<Nav className="ml-auto">
					{rightNav}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}
