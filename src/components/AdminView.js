import {useEffect, useState, Fragment} from 'react';
import {Table, Modal, Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){
	const {productData,fetchData} = props
	const [products, setProducts]=useState('');
	const [name, setName]=useState('');
	const [description, setDescription]=useState('');
	const [price, setPrice]=useState(0);
	const [productId, setProductId] = useState('');
	const [showAdd, setShowAdd] = useState(false);
	const [showMod, setShowMod] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);
	const openMod = (productId) => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res=>res.json())
		.then(data=> {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
		setShowMod(true)
	};
	const closeMod = () => {
		setName('');
		setDescription('');
		setPrice('');
		setShowMod(false);
	};


	useEffect(() => {
		const productArray = productData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.createdOn}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? 'text-success' : 'text-muted'}>
						{product.isActive? 'Available' : 'Unavailable'}
					</td>
					<td>
						<Button variant='info' onClick = {()=>openMod(product._id)}>Update</Button>
					</td>
					<td>
						{product.isActive?
							<Button variant='warning' onClick = {()=>archiveProduct(product._id, product.isActive)}>Archive</Button>
							:
							<Button variant='success' onClick = {()=>unarchiveProduct(product._id, product.isActive)}>Unarchive</Button>
						}
					</td>
				</tr>
			)
			
		})
		setProducts(productArray)
	},[productData])

	const addProduct = e => {
		e.preventDefault();
		fetch('http://localhost:4000/products/createProduct', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res=>res.json())
		.then(data=> {
			console.log(data)
			if (data===true) {
				fetchData();

				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: `${name} has been added.`
				})
				setName('');
				setDescription('');
				setPrice(0);

				closeAdd()
			} else {
				Swal.fire({
					title: 'Failed',
					icon: 'error',
					text: `Something went wrong please try again.`
				})
			}
		})
	};

	const modifyProduct = (e,productId) => {
		e.preventDefault();
		fetch(`http://localhost:4000/products/modify/${productId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res=>res.json())
		.then(data=> {
			if (data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product has been updated.'
				})
				fetchData();
				closeMod();
			} else {
				fetchData();
				Swal.fire({
					title: 'Failed',
					icon: 'error',
					text: 'Something went wrong. Please try again.'
				})
			}
		})
	}

	const archiveProduct = (productId, IsActive) => {
		fetch(`http://localhost:4000/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}` 
			},
			body: JSON.stringify({
				IsActive:IsActive
			})
		})
		.then(res=>res.json())
		.then(data=> {
			if (data) {
				fetchData();
				Swal.fire({
					title:'Success',
					icon:'success',
					text:'Product successfully disabled'
				})
			} else {
				fetchData();
				Swal.fire({
					title:'Something went wrong',
					icon:'error',
					text:'Please Try Again'
				})
			}
		})
	}

	const unarchiveProduct = (productId, IsActive) => {
		fetch(`http://localhost:4000/products/unarchive/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}` 
			},
			body: JSON.stringify({
				IsActive:IsActive
			})
		})
		.then(res=>res.json())
		.then(data=> {
			if (data) {
				fetchData();
				Swal.fire({
					title:'Success',
					icon:'success',
					text:'Course successfully disabled'
				})
			} else {
				fetchData();
				Swal.fire({
					title:'Something went wrong',
					icon:'error',
					text:'Please Try Again'
				})
			}
		})
	}

	return(
		<Fragment>
			<div className='d-flex justify-content-center'>
				<h2>Admin Dashboard</h2>
			</div>
			<Table>
				<thead>
					<tr>
						<td>id</td>
						<td>Date Added</td>
						<td>Name</td>
						<td>Description</td>
						<td>Price</td>
						<td>Availability</td>
						<td>Update</td>
						<td>Archive</td>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
			
			<div className="d-flex">
				<Button variant="primary" onClick={openAdd}> Add New Product </Button>
			</div>

		{/*Start of Modal for Adding Product*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e=> addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		{/*End of Modal for Adding Product*/}

		{/*Start of Modal for Modifying Product*/}
			<Modal show={showMod} onHide={closeMod}>
				<Form onSubmit={e=> modifyProduct(e, productId)}>

					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeMod}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		{/*End of Modal for Modifying Product*/}

		</Fragment>
	)
}