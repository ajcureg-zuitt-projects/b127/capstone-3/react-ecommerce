import {useState, useEffect, Fragment} from 'react';
import {Table, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2';


export default function UserCart(props){
	const {productData, productPrice, fetchProduct} = props
	const [hasContent,setHasContent] = useState('')
	const [cartProduct, setCartProduct] = useState([]);

	const removeProduct = (productId) => {
		fetch('http://localhost:4000/cart/removeProduct', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res=>res.json())
		.then(data=> {
			if (data) {
				fetchProduct();
				Swal.fire({
					title: 'Removed',
					icon: 'warning',
				})
			} else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: `Something went wrong. Please try again`
				})
			}
		})
	}

	const makeOrder = e => {
		fetch('http://localhost:4000/orders/createOrder', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}

		})
		.then(res=>res.json())
		.then(data=> {
			if (data) {
				fetchProduct();
				Swal.fire({
					title: 'Order Successful',
					icon: 'success',
					text: `An order has been successfully made.`
				})
			} else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: `Something went wrong. Please try again`
				})
			}
		})
	}

	useEffect(()=> {
		if (productData.length===0) {
			setHasContent(false);
		} else {
			const cartArr = productData.map(product=> {
				return(
					<tr key={product.productId}>
	 					<td>{product.addedOn.slice(0,10)}</td>
	 					<td>{product.name}</td>
						<td>{product.quantity}</td>
						<td>{product.totalPerProduct}</td>
						<td>
							<Button variant='danger' onClick={()=>removeProduct(product.productId)}>Remove</Button>
						</td>
	 				</tr>
				)
			})
			setHasContent(true);
			setCartProduct(cartArr);
		}
	},[productData])
	return(

		(hasContent) ?
		<Fragment>
			<div className='d-flex justify-content-center'>
				<h2>Your Cart</h2>
			</div>
			<Table>
				<thead>
					<tr>
						<td>Date Added</td>
						<td>Name</td>
						<td>Quantity</td>
						<td>Total</td>
					</tr>
				</thead>
				<tbody>
					{cartProduct}
				</tbody>
				<tfoot>
					<tr>
						<td colSpan='3' className='text-center'>Total</td>
						<td>{productPrice}</td>
						<td>
							<Button variant='primary' onClick={e=>makeOrder(e)}>Checkout</Button>
					
						</td>	
					</tr>
				</tfoot>
			</Table>
		</Fragment>
		:
		<Fragment>
			<div className='text-center justify-content-center pt-5'>
				<h4>Cart is Empty!</h4>
				<p>Click <Link to='/products'>here</Link> to view available products.</p>
			</div>
		</Fragment>
	)
}